#include <iostream>
#include <cstdlib>

class IndexException {};
class OutOfMemoryException {};

class TList {
private:
    int* pointer;
    int current;
    int size;

    void expand(int newSize) {
        int* temp;
        newSize = newSize + this->size / 5;

        temp = (int*)realloc(this->pointer, sizeof(int) * newSize);

        if ( temp != NULL ) {
            this->pointer = temp;
            this->size = newSize;
        } else {
            throw OutOfMemoryException();
        }
    }

public:
    // friend std::ostream& operator<<(std::ostream& os, const TList& lst);
    
    TList(int size=10) {
        this->pointer = (int*)malloc(sizeof(int) * size);
        this->current = 0;
        this->size = size;

        if ( DEBUG ) {
            std::cout << "TList(int size=10)" << std::endl;
        }
    }

    void report() const {
        std::cout << "-----------" << std::endl;
        std::cout << "Pointer is: " << this->pointer << std::endl;
        std::cout << "Size is: " << this->size << std::endl;
        std::cout << "Current is: " << this->current << std::endl;
        std::cout << "-----------" << std::endl;

        if ( DEBUG ) {
            std::cout << "report()" << std::endl;
        }
    }

    void print() const {
        int last = this->current - 1;

        for ( int i = 0; i < last; i++ ) {
            std::cout << this->pointer[i] << ' ';
        }
        std::cout << this->pointer[last] << std::endl;

        if ( DEBUG ) {
            std::cout << "print()" << std::endl;
        }
    }

    void fillByZero() {
        for ( int i = 0; i < this->size; i++ ) {
            this->pointer[i] = 0;
        }

        if ( DEBUG ) {
            std::cout << "fillByZero()" << std::endl;
        }
    }

    void clear() {
        this->fillByZero();
        this->current = 0;
    }

    int count(int element) const {
        int counter = 0;

        for ( int i = 0; i < this->current; i++ ) {
            if ( this->pointer[i] == element ) {
                counter += 1;
            }
        }

        return counter;
    }

    void add(int value, int amount=1) {
        int newSize = this->current + amount;

        if ( newSize > this->size ) {
            this->expand(newSize);
        }

        for ( int i = 0; i < amount; i++ ) {
            this->pointer[this->current] = value;
            this->current += 1;
        }
    }

    int* getPointer() const {
        return this->pointer;
    }

    int getCurrent() const {
        return this->current;
    }

    int getSize() const {
        return this->size;
    }

    int& operator[](int index) {
        if ( index >= this->size ) {
            throw IndexException();
        }

        return this->pointer[index];
    }

    int& operator*() const {
        return this->pointer[this->current - 1];
    }

    void operator()() const {
        this->report();
    }

    void operator()(int a) const {
        std::cout << "Uno" << std::endl;
    }

    int operator()(int a, int b) const {
        return 42;
    }

    ~TList() {
        free(this->pointer);
        this->size = 0;
        this->pointer = NULL;

        if (DEBUG) {
            std::cout << "~TList()" << std::endl;
        }
    }
};

std::ostream& operator<<(std::ostream& os, const TList& lst) {
    int last = lst.getCurrent() - 1;
    int* temp = lst.getPointer();

    for ( int i = 0; i < last; i++ ) {
        os << temp[i] << ' ';
    }
    os << temp[last];

    return os;
}