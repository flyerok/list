class IndexException {};
class OutOfMemoryException {};

class ArrayList {
private:
    int* pointer;
    int current;
    int size;

    void expand(int newSize);

public:
    ArrayList(int size=10);

    void report() const;
    void print() const;

    void clear();
    int  count(int element) const;
    void add(int value, int amount=1);

    int* getPointer() const;
    int getCurrent() const;
    int getSize() const;

    int& operator[](int);

    ~ArrayList();
};

std::ostream& operator<<(std::ostream& os, const ArrayList& lst);