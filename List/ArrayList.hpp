class IndexException {};
class OutOfMemoryException {};

class ArrayList {
private:
    int* pointer;
    int current;
    int size;

    void expand(int newSize);

public:
    // friend std::ostream& operator<<(std::ostream& os, const ArrayList& lst);
    
    ArrayList(int size=10);

    void report() const;
    void print() const;

    void fillByZero();
    void clear();
    int  count(int element) const;
    void add(int value, int amount=1);

    int* getPointer() const;
    int getCurrent() const;
    int getSize() const;

    int& operator[](int);

    void operator()() const;
    void operator()(int) const;
    int operator()(int, int) const;

    int& operator*() const;

    ~ArrayList();
};

std::ostream& operator<<(std::ostream& os, const ArrayList& lst);