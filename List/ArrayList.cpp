#include <iostream>
#include <cstdlib>
#include "ArrayList.hpp"

ArrayList::ArrayList(int size) {
    this->pointer = (int*)malloc(sizeof(int) * size);
    this->current = 0;
    this->size = size;

    if ( DEBUG ) {
        std::cout << "ArrayList(int size=10)" << std::endl;
    }
}

void ArrayList::expand(int newSize) {
    int* temp;
    newSize = newSize + this->size / 5;

    temp = (int*)realloc(this->pointer, sizeof(int) * newSize);

    if ( temp != NULL ) {
        this->pointer = temp;
        this->size = newSize;
    } else {
        throw OutOfMemoryException();
    }
}


void ArrayList::report() const {
    std::cout << "-----------" << std::endl;
    std::cout << "Pointer is: " << this->pointer << std::endl;
    std::cout << "Size is: " << this->size << std::endl;
    std::cout << "Current is: " << this->current << std::endl;
    std::cout << "-----------" << std::endl;

    if ( DEBUG ) {
        std::cout << "ArrayList::report()" << std::endl;
    }
}

void ArrayList::print() const {
    int last = this->current - 1;

    for ( int i = 0; i < last; i++ ) {
        std::cout << this->pointer[i] << ' ';
    }
    std::cout << this->pointer[last] << std::endl;

    if ( DEBUG ) {
        std::cout << "ArrayList::print()" << std::endl;
    }
}

void ArrayList::fillByZero() {
    for ( int i = 0; i < this->size; i++ ) {
        this->pointer[i] = 0;
    }

    if ( DEBUG ) {
        std::cout << "ArrayList::fillByZero()" << std::endl;
    }
}

void ArrayList::clear() {
    this->fillByZero();
    this->current = 0;
}

int ArrayList::count(int element) const {
    int counter = 0;

    for ( int i = 0; i < this->current; i++ ) {
        if ( this->pointer[i] == element ) {
            counter += 1;
        }
    }

    return counter;
}

void ArrayList::add(int value, int amount) {
    int newSize = this->current + amount;

    if ( newSize > this->size ) {
        this->expand(newSize);
    }

    for ( int i = 0; i < amount; i++ ) {
        this->pointer[this->current] = value;
        this->current += 1;
    }
}

int* ArrayList::getPointer() const {
    return this->pointer;
}

int ArrayList::getCurrent() const {
    return this->current;
}

int ArrayList::getSize() const {
    return this->size;
}

int& ArrayList::operator[](int index) {
    if ( index >= this->size ) {
        throw IndexException();
    }

    return this->pointer[index];
}

int& ArrayList::operator*() const {
    return this->pointer[this->current - 1];
}

void ArrayList::operator()() const {
    this->report();
}

void ArrayList::operator()(int a) const {
    std::cout << "Uno" << std::endl;
}

int ArrayList::operator()(int a, int b) const {
    return 42;
}

ArrayList::~ArrayList() {
    free(this->pointer);
    this->size = 0;
    this->pointer = NULL;

    if (DEBUG) {
        std::cout << "~ArrayList()" << std::endl;
    }
}

std::ostream& operator<<(std::ostream& os, const ArrayList& lst) {
    int last = lst.getCurrent() - 1;
    int* temp = lst.getPointer();

    for ( int i = 0; i < last; i++ ) {
        os << temp[i] << ' ';
    }
    os << temp[last] << std::endl;

    return os;
}
