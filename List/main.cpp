#include <iostream>
#include "ArrayList.hpp"

int main() {
    ArrayList* ololo = new ArrayList();
    ArrayList& lst = *ololo;

    lst.add(42);
    *lst = 45;

    // try {
    //     lst[500] = 10;
    // } catch (...) {
    //     // std::cout << "Error" << std::endl;
    //     // lst[1] = 10;
    // }


    std::cout << *lst << std::endl;

    lst();
    lst(5);
    std::cout << lst(5, 4) << std::endl;

    std::cout << lst << std::endl;

    delete ololo;

    return 0;
}
